import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: colors.grey.darken3,
        accent: colors.grey.darken3,
        secondary: "#272727",
        info: colors.teal.lighten1,
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3,
        background: "#303030", // Not automatically applied
      },
      light: {
        accent: colors.blueGrey.lighten5,
        primary: colors.blueGrey.darken1,
        secondary: colors.blueGrey.lighten4,
        background: colors.grey.lighten2, // Not automatically applied
      },
    },
  },
});
