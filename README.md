# Cv Template with linkedin and vuejs

This application allows you to use linkedin to modify your cv site.

To comment, you will need to retrieve the cookies from your linkedin by following this tutorial : https://github.com/linkedtales/scrapedin/wiki/Using-Cookies-To-Login

I use the scrapedin nodejs library to do that : https://github.com/linkedtales/scrapedin and vuejs.

The app is an VueJs [Pwa](https://www.goodbarber.com/blog/pwas-progressive-web-apps-explained-a825/) wich use [vuetify](https://vuetifyjs.com/en/). You can install this app on your smartphone. 

To see an exemple, let's see this [website](romainleduc.com).

They also have a [dockerfile](https://www.docker.com/) and a [gitlab ci](https://docs.gitlab.com/ee/ci/) that you can use.

## Project setup
```
cd front 
npm install

cd back
npm install

```

### Compiles and hot-reloads for development
```
cd front
npm start

cd back
npm start
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).