import axios from "axios";

const API_URL =
  process.env.VUE_APP_URL_API + "api/getprofile/";

class ProfileService {
  async getProfile() {
    return await axios.get(API_URL);
  }
}

export default new ProfileService();
