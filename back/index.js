const express = require("express");
const app = express();
const scrapedin = require("scrapedin");
const bodyParser = require("body-parser");
const cors = require("cors");
const JsonDB = require("node-json-db");
const fs = require("fs");
require("dotenv-flow").config();

var db = new JsonDB("data", true, false, "/");

async function getProfile() {
  try {
    const cookies = fs.readFileSync("./cookieFile.json");
    const options = {
      cookies: JSON.parse(cookies),
    };
    const profileScraper = await scrapedin(options);
    const profile = await profileScraper(
      "https://www.linkedin.com/in/romain-le-duc/"
    );
    formatProfile(profile);
    console.log("Insert in DB");
  } catch (error) {
    console.log(error);
  }
}

function formatProfile(profile) {
  var profileFormat = {};

  for (var pos in profile.positions) {
    var obj = profile.positions[pos];

    if (obj.roles) {
      for (var role in obj) {
        var obj2 = obj[role];
        profileFormat.position = obj2;
      }
    } else if (!obj.roles) {
      profileFormat["position"].push(obj);
    }
  }
  profileFormat.profile = profile.profile;
  insertData(profileFormat);
}

function insertData(profile) {
  db.push("/", profile);
}
var corsOptions = {
  origin: process.env.X_ORIGIN_URL,
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/api/getprofile", function (req, res) {
  const profile = db.getData("/");
  res.send(profile);
  console.log("Send");
});

var port = process.env.PORT || '4000'
app.listen(port, function () {
  getProfile();
  console.log("Example app listening on port "+process.env.PORT+" !");
});
